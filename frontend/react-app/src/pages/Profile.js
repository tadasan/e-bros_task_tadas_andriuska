import React, { useState, useEffect } from "react";
import Cookie from "js-cookie";
import { useHistory } from "react-router-dom";
import axios from "axios";

export default function Profile() {
  const [user, setUser] = useState(null);
  const history = useHistory();

  useEffect(() => {
    axios
      .get("http://localhost:4000/users/me", {
        headers: {
          "x-auth-token": Cookie.get("Authorization")
        }
      })
      .then(res => {
        setUser(res.data);
      })
      .catch(() => {
        Cookie.remove("Authorization");
        history.push("/login");
      });
  }, [history]);

  return (
    <div>
      {user && (
        <div>
          <p>Hello {user.email} !</p>
          <button
            onClick={() => {
              Cookie.remove("Authorization");
              setUser(null);
              history.push("/login");
            }}
          >
            Logout
          </button>
        </div>
      )}
    </div>
  );
}
