const express = require("express");
const cors = require("cors");
const app = express();
const users = require("./routes/users");
const mongoose = require("mongoose");
const error = require("./middleware/error");

process.on("uncaughtException", ex => {
  //ex could be logged using winston
  console.log("Got an uncaught exception");
});

process.on("unhandledRejection", ex => {
  //ex could be logged using winston
  console.log("Got an unhandled rejection");
});

mongoose
  .connect("mongodb://localhost:27017/ebros", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => console.log("connected to database"))
  .catch(err => console.log("error:", err));

app.use(express.json());
app.use(cors());
app.use(users);
app.use(error);

const port = 4000;
const server = app.listen(port, () => {
  console.log("listening on port ", port);
});
