import React from "react";
import * as Yup from "yup";
import { Formik } from "formik";

export default function SignUpForm({ onSubmit }) {
  const VALIDATION_SCHEMA = Yup.object().shape({
    email: Yup.string()
      .email("Please enter valid email")
      .required("Please enter your email"),
    password: Yup.string()
      .required("Please enter password")
      .min(8, "Password must be at least 8 symbols long"),
    confirmPassword: Yup.string()
      .required("Passwords do not match")
      .oneOf([Yup.ref("password")], "Passwords do not match")
  });

  return (
    <Formik
      validateOnChange={false}
      initialValues={{
        email: "",
        password: "",
        confirmPassword: ""
      }}
      onSubmit={onSubmit}
      validationSchema={VALIDATION_SCHEMA}
    >
      {({ values, errors, handleChange, handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div>
            <label>Email</label><br/>
            <input
              id="email"
              name="email"
              autoComplete="off"
              type="text"
              value={values.email}
              onChange={handleChange}
            />
            {errors.email && (
              <div id="feedback" style={{ color: "red" }}>
                {errors.email}
              </div>
            )}
          </div>

          <div>
            <label>Password</label><br/>
            <input
              id="password"
              name="password"
              autoComplete="off"
              type="password"
              value={values.password}
              onChange={handleChange}
            />
            {errors.password && (
              <div id="feedback" style={{ color: "red" }}>
                {errors.password}
              </div>
            )}
          </div>

          <div>
            <label>Confirm password</label><br/>
            <input
              id="confirmPassword"
              name="confirmPassword"
              autoComplete="off"
              type="password"
              value={values.confirmPassword}
              onChange={handleChange}
            />
            {errors.confirmPassword && (
              <div id="feedback" style={{ color: "red" }}>
                {errors.confirmPassword}
              </div>
            )}
          </div>
          <button type="submit">Log in</button>
        </form>
      )}
    </Formik>
  );
}

// LogInForm.propTypes = {
//   onSubmit: PropTypes.func,
// };
