import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Cookie from "js-cookie";
import axios from "axios";
import SignUpForm from "../features/forms/SignUpForm";

export default function Signup() {
  const [signupError, setError] = useState(null);
  const [signupStatus, setSignup] = useState(false);
  const history = useHistory();

  useEffect(() => {
    axios
      .get("http://localhost:4000/users/me", {
        headers: {
          "x-auth-token": Cookie.get("Authorization")
        }
      })
      .then(res => {
        history.push("/profile");
      })
      .catch(() => {});
  }, [history]);

  const handleSubmit = values => {
    axios
      .post("http://localhost:4000/users", {
        email: values.email,
        password: values.password
      })
      .then(() => {
        setSignup(true);
        setTimeout(() => {
          history.push("/login");
        }, 2000);
      })
      .catch(error => {
        setError(error.response.data);
      });
  };

  return (
    <div>
      <SignUpForm onSubmit={handleSubmit} />
      {signupStatus && (
        <div style={{ color: "green" }}>
          Success. Redirecting to Login page..
        </div>
      )}
      {signupError && <div style={{ color: "red" }}>{signupError}</div>}
    </div>
  );
}
