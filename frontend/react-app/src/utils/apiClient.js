import axios from 'axios';

const apiClient = axios.create({
  baseURL: process.env.REACT_APP_BE_BASE_URL,
  timeout: 15000,
}
);

export default apiClient;
