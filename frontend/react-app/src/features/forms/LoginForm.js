import React from "react";
import * as Yup from "yup";
import { Formik } from "formik";

export default function LoginForm({ onSubmit }) {
  const VALIDATION_SCHEMA = Yup.object().shape({
    email: Yup.string()
      .email("Email is not valid")
      .required("Please enter your email"),
    password: Yup.string().required("Please enter your password")
  });

  return (
    <Formik
      validateOnChange={false}
      initialValues={{
        email: "",
        password: ""
      }}
      onSubmit={onSubmit}
      validationSchema={VALIDATION_SCHEMA}
    >
      {({ values, errors, handleChange, handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div>
            <label>Email</label>
            <br />
            <input
              id="email"
              name="email"
              autoComplete="off"
              type="text"
              value={values.email}
              onChange={handleChange}
            />
            {errors.email && (
              <div id="feedback" style={{ color: "red" }}>
                {errors.email}
              </div>
            )}
          </div>

          <div>
            <label>Password</label>
            <br />
            <input
              id="password"
              name="password"
              autoComplete="off"
              type="password"
              value={values.password}
              onChange={handleChange}
            />
            {errors.password && (
              <div id="feedback" style={{ color: "red" }}>
                {errors.password}
              </div>
            )}
          </div>
          <button type="submit">Log in</button>
        </form>
      )}
    </Formik>
  );
}

// LogInForm.propTypes = {
//   onSubmit: PropTypes.func,
// };
