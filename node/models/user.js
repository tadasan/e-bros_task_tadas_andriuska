const Joi = require("@hapi/joi");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    min: 8
  }
});

userSchema.methods.generateAuthToken = function() {

  //TODO: Use env variables for storing jwtSecretKey
  const token = jwt.sign({ _id: this._id }, "myLovelyKey");
  return token;
};

const User = mongoose.model("User", userSchema);

function validateUser(user) {
  const schema = {
    email: Joi.string().email(),
    password: Joi.string().required().min(8)
  };
  return Joi.validate(user, schema);
}

exports.User = User;
exports.validateUser = validateUser;
