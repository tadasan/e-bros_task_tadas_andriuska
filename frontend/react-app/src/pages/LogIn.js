import React, { useState, useEffect } from "react";
import Cookie from "js-cookie";
import { useHistory } from "react-router-dom";
import axios from "axios";
import LoginForm from "../features/forms/LoginForm";

export default function Login() {
  const [loginError, setError] = useState(null);
  const [loginStatus, setLogin] = useState(false);
  const history = useHistory();

  useEffect(() => {
    axios
      .get("http://localhost:4000/users/me", {
        headers: {
          "x-auth-token": Cookie.get("Authorization")
        }
      })
      .then(res => {
        history.push("/profile");
      })
      .catch(error => {});
  }, [loginStatus, history]);

  const handleSubmit = values => {
    axios
      .post("http://localhost:4000/users/login", {
        email: values.email,
        password: values.password
      })
      .then(res => {
        const jwt = res.data;
        Cookie.set("Authorization", jwt);
        setLogin(true);
      })
      .catch(error => {
        setError(error.response.data);
      });
  };

  return (
    <div>
      <LoginForm onSubmit={handleSubmit} />
      {loginError && <div style={{ color: "red" }}>{loginError}</div>}
    </div>
  );
}
